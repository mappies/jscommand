import {assert as assert} from 'chai';
import {Mock, It} from "typemoq";
import { ICommand } from '../src/interfaces/iCommand';
import { Command } from '../src/entities/command';
import { IfCommand } from '../src/entities/ifCommand';
import { IRevertableCommand } from '../src/interfaces/iRevertableCommand';
import { FailSafeCommand } from '../src/entities/failSafeCommand';

describe('FailSafeCommand', () => 
{  
    it('execute() - command not throw an error', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new FailSafeCommand(mockedCommand.object).execute();
        
        assert.isTrue(called);
    });

    it('execute() - command throws an error', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true).throws(new Error());
        
        await new FailSafeCommand(mockedCommand.object).execute();
        
        assert.isTrue(called);
    });

    it('revert() - not revertable command', async () =>
    { 
        let mockedCommand = Mock.ofType<ICommand>();
        
        let command =  new FailSafeCommand(mockedCommand.object);

        let error;

        try
        {
            await command.execute();
            await command.revert();
        }
        catch(e)
        {
            error = e;
        }

        assert.isUndefined(error);
    });

    it('revert()', async () =>
    {   
        let called = false;

        let mockedCommand = Mock.ofType<IRevertableCommand>();
        mockedCommand.setup(c => c.revert(It.isAny())).callback(()=>called=true);
        
        let command =  new FailSafeCommand(mockedCommand.object);

        await command.execute();
        assert.isFalse(called);

        await command.revert();
        assert.isTrue(called);
    });
});

