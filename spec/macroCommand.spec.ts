import {assert as assert} from 'chai';
import {Mock, It} from "typemoq";
import { MacroCommand } from '../src/entities/macroCommand';
import { ICommand } from '../src/interfaces/iCommand';

describe('MacroCommand', () => 
{  
    it('execute()', async () =>
    {
        let cmd1 = Mock.ofType<ICommand>();
        cmd1.setup(c => c.execute(undefined)).returns(async ()=>true);

        let cmd2 = Mock.ofType<ICommand>();
        cmd2.setup(c => c.execute(true)).returns(async ()=>["a"]);

        let cmd3 = Mock.ofType<ICommand>();
        cmd3.setup(c => c.execute(["a"])).returns(async ()=>3.14);

        let cmd = new MacroCommand().add(cmd1.object).add(cmd2.object).add(cmd3.object);

        let result = await cmd.execute();
        assert.equal(result, 3.14);
    });

    it('execute() - should stop when an error occurs', async () =>
    {
        let pipelineCompleted = false;

        let cmd1 = Mock.ofType<ICommand>();
        cmd1.setup(c => c.execute(undefined)).returns(async ()=>true);

        let cmd2 = Mock.ofType<ICommand>();
        cmd2.setup(c => c.execute(true)).throws(new Error('Simulated Error'));

        let cmd3 = Mock.ofType<ICommand>();
        cmd3.setup(c => c.execute(It.isAny())).callback(()=>pipelineCompleted = true);

        let cmd = new MacroCommand();
        cmd.add(cmd1.object);
        cmd.add(cmd2.object);
        cmd.add(cmd3.object);

        let error = undefined;

        try
        {
            await cmd.execute();
            assert.fail('This statement should not be executed.');
        }
        catch(e)
        {
            error = e;
        }

        assert.instanceOf(error, Error);
        assert.equal(error.message, 'Simulated Error');
        assert.isFalse(pipelineCompleted);
    });

    it('execute() - work with a promise', async () =>
    {
        let pipelineCompleted = false;

        let cmd1 = Mock.ofType<ICommand>();
        cmd1.setup(c => c.execute(undefined)).returns(async ()=>true);

        let cmd2 = Mock.ofType<ICommand>();
        cmd2.setup(c => c.execute(true)).returns(()=>new Promise((resolve)=>setTimeout(() =>resolve(2), 10)));

        let cmd3 = Mock.ofType<ICommand>();
        cmd3.setup(c => c.execute(2)).returns(async ()=>"done")

        let cmd = new MacroCommand();
        cmd.add(cmd1.object);
        cmd.add(cmd2.object);
        cmd.add(cmd3.object);

        let result = await cmd.execute();
        
        assert.equal(result, "done");
    });
});