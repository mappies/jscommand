import {assert as assert} from 'chai';
import { Command } from '../src/entities/command';
import { RetryableCommand } from '../src/entities/retryableCommand';
import * as AggregateError from 'aggregate-error';

class MyError extends Error
{
    constructor(m:string)
    {
        super(m);
    }
}

class TestCommand extends Command 
{
    count:number = 0;
    result:string = '';

    action(data: any = ''):any {
        this.result += '.'
        if(this.count++ < 2) throw new MyError('err!');
        return this.result;
    }
}

describe('RetryableCommand', () => 
{  
    it('execute() - Retried successfully', async () =>
    {   
        assert.equal(await new RetryableCommand(new TestCommand(), MyError, 3).execute(), '...');
        assert.equal(await new RetryableCommand(new TestCommand(), MyError, 5).execute(), '...');
    });

    it('execute() - Retried unsuccessfully', async () =>
    {   
        let error;

        try
        {
            await new RetryableCommand(new TestCommand(), MyError, 1).execute();
        }
        catch(e)
        {
            error = e;
        }

        assert.isDefined(error);
        assert.isTrue(error instanceof AggregateError);
    });

    it('execute() - Retried successfully (2)', async () =>
    {   
        assert.equal(await new RetryableCommand(new TestCommand(), MyError, 2).execute(), '...');
    });

    it('execute() - Did not retry when a wrong error is thrown', async () =>
    {   
        let error;

        try
        {
            await new RetryableCommand(new TestCommand(), 'invalid', 10).execute();
        }
        catch(e)
        {
            error = e;
        }

        assert.isDefined(error);
        assert.isTrue(error instanceof Error);
        assert.isFalse(error instanceof AggregateError);
    });
});

