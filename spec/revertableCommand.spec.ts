import {assert as assert} from 'chai';
import { RevertableCommand } from '../src/entities/revertableCommand';
import * as AggregateError from 'aggregate-error';

class TruthCommand extends RevertableCommand 
{
    isReverted:boolean = false;

    revert(data?: any):any 
    {
        this.isReverted = true;
    }

    action(data?: any):any 
    {
        if(data === true) return data;

        throw new Error(`${data} is not true.`);
    }
}

class FailToRevertCommand extends RevertableCommand 
{
    revert(data?: any):any 
    {
        throw new Error('Revert failed.');
    }

    action(data?: any):any 
    {
        throw new Error(`Action failed.`);
    }
}

class AsyncRevertableCommand extends RevertableCommand
{
    async action(): Promise<number>{
        return new Promise<number>((resolve, reject)=>{
            setTimeout(()=>reject(new Error('Simulated Error')), 10);
        });
    }

    async revert(): Promise<number>{
        return new Promise<number>((resolve)=>{
            setTimeout(()=>resolve(5), 10);
        });
    }
}

describe('RevertableCommand', () => 
{  
    it('execute()', () =>
    {   
        assert.equal(new TruthCommand().execute(true), true);
    });

    it('revert()', () =>
    {   
        let error = undefined;

        try
        {
            new TruthCommand().execute(false);
        }
        catch(e)
        {
            error = e;
        }

        assert.instanceOf(error, Error);
        assert.equal(error.message, 'false is not true.');
    });

    it('revert() - an error in revert() should also be caught.', () =>
    {   
        let error = undefined;

        try
        {
            new FailToRevertCommand().execute();
        }
        catch(e)
        {
            error = e;
        }

        assert.instanceOf(error, AggregateError);

        assert.equal([...error][0].message, "Action failed.")
        assert.equal([...error][1].message, "Revert failed.")
    });

    it('revert() - work with a promise', async () =>
    {   
        let error = undefined;

        try
        {
            await new AsyncRevertableCommand().execute();
        }
        catch(e)
        {
            error = e;
        }
        assert.instanceOf(error, Error)
        assert.equal(error.message, "Simulated Error");
    });
});