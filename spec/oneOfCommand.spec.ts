import {assert as assert} from 'chai';
import {Mock, It} from "typemoq";
import { OneOfCommand } from '../src/entities/oneOfCommand';
import { ICommand } from '../src/interfaces/iCommand';
import * as AggregateError from 'aggregate-error';

describe('OneOfCommand', () => 
{  
    let cmd1executed:boolean;
    let cmd2executed:boolean;
    let cmd3executed:boolean;

    beforeEach(()=>
    {
        cmd1executed = cmd2executed = cmd3executed = false;
    });

    it('execute()', async () =>
    {
        let cmd1 = Mock.ofType<ICommand>();
        cmd1.setup(c => c.execute(3.14)).callback(()=>cmd1executed=true).throws(new Error('Simulated Error 1'));

        let cmd2 = Mock.ofType<ICommand>();
        cmd2.setup(c => c.execute(3.14)).callback(()=>cmd2executed=true).returns(async ()=>42);

        let cmd3 = Mock.ofType<ICommand>();
        cmd3.setup(c => c.execute(It.isAny())).callback(()=>cmd3executed=true).returns(async ()=>true);

        let cmd = new OneOfCommand().add(cmd1.object).add(cmd2.object).add(cmd3.object);

        let result = await cmd.execute(3.14);
        assert.equal(result, 42);

        assert.isTrue(cmd1executed);
        assert.isTrue(cmd2executed);
        assert.isFalse(cmd3executed);
    });

    it('execute() - should throw an error when all commands failed', async () =>
    {
        let cmd1 = Mock.ofType<ICommand>();
        cmd1.setup(c => c.execute(3.14)).callback(()=>cmd1executed=true).throws(new Error('Simulated Error 1'));

        let cmd2 = Mock.ofType<ICommand>();
        cmd2.setup(c => c.execute(3.14)).callback(()=>cmd2executed=true).throws(new Error('Simulated Error 2'));

        let cmd3 = Mock.ofType<ICommand>();
        cmd3.setup(c => c.execute(3.14)).callback(()=>cmd3executed=true).throws(new Error('Simulated Error 3'));

        let cmd = new OneOfCommand().add(cmd1.object).add(cmd2.object).add(cmd3.object);

        let error = undefined;

        try
        {
            await cmd.execute(3.14);
            assert.fail('This statement should not be executed.');
        }
        catch(e)
        {
            error = e;
        }

        assert.instanceOf(error, AggregateError);
        assert.notEqual(error.message.indexOf('Simulated Error 1'), -1);
        assert.notEqual(error.message.indexOf('Simulated Error 2'), -1);
        assert.notEqual(error.message.indexOf('Simulated Error 3'), -1);
       
        assert.isTrue(cmd1executed);
        assert.isTrue(cmd2executed);
        assert.isTrue(cmd3executed);
    });

    it('execute() - work with a promise', async () =>
    {
        let pipelineCompleted = false;

        let cmd1 = Mock.ofType<ICommand>();
        cmd1.setup(c => c.execute(undefined)).callback(()=>cmd1executed=true).throws(new Error('Simulated Error 1'));

        let cmd2 = Mock.ofType<ICommand>();
        cmd2.setup(c => c.execute(undefined)).callback(()=>cmd2executed=true).returns(()=>new Promise((resolve)=>setTimeout(() =>resolve(2), 10)));

        let cmd3 = Mock.ofType<ICommand>();
        cmd3.setup(c => c.execute(It.isAny())).callback(()=>cmd3executed=true).returns(async ()=>"done")

        let cmd = new OneOfCommand();
        cmd.add(cmd1.object);
        cmd.add(cmd2.object);
        cmd.add(cmd3.object);

        let result = await cmd.execute();
        
        assert.equal(result, 2);
        assert.isTrue(cmd1executed);
        assert.isTrue(cmd2executed);
        assert.isFalse(cmd3executed);
    });
});