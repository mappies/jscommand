import {assert as assert} from 'chai';
import {Mock, It} from "typemoq";
import { IRevertableCommand } from '../src/interfaces/iRevertableCommand';
import { RevertableMacroCommand } from '../src/entities/revertableMacroCommand';
import { ICommand } from '../src/interfaces/iCommand';

describe('RevertableMacroCommand', () => 
{  
    it('execute()', async () =>
    {
        let cmd1 = Mock.ofType<ICommand>();
        cmd1.setup(c => c.execute(undefined)).returns(async ()=>true);

        let cmd2 = Mock.ofType<ICommand>();
        cmd2.setup(c => c.execute(true)).returns(async ()=>["a"]);

        let cmd3 = Mock.ofType<ICommand>();
        cmd3.setup(c => c.execute(["a"])).returns(async ()=>3.14);

        let cmd = new RevertableMacroCommand().add(cmd1.object).add(cmd2.object).add(cmd3.object);

        let result = await cmd.execute();
        assert.equal(result, 3.14);
    });

    it('execute() - should stop and revert when an error occurs', async () =>
    {
        let pipelineCompleted = false;
        let cmd1reverted = false;
        let cmd3reverted = false;
        let cmd4reverted = false;
        let cmd5reverted = false;

        let cmd1 = Mock.ofType<IRevertableCommand>();
        cmd1.setup(c => c.execute(undefined)).returns(async ()=>3);
        cmd1.setup(c => c.revert).returns(()=>(a?:any)=>a);
        cmd1.setup(c => c.revert(7)).callback(async()=>cmd1reverted=true);

        let cmd2 = Mock.ofType<ICommand>();
        cmd2.setup(c => c.execute(3)).returns(async ()=>false);
        cmd2.setup(c => c['revert']).returns(()=>undefined);

        let cmd3 = Mock.ofType<IRevertableCommand>();
        cmd3.setup(c => c.execute(false)).returns(async ()=>({id:1}));
        cmd3.setup(c => c.revert).returns(()=>(a?:any)=>a);
        cmd3.setup(c => c.revert({id:1})).callback(async()=>cmd3reverted=true).returns(async()=>7);

        let cmd4 = Mock.ofType<IRevertableCommand>();
        cmd4.setup(c => c.execute({id:1})).throws(new Error('Simulated Error'));
        cmd4.setup(c => c.revert).returns(()=>(a?:any)=>a);
        cmd4.setup(c => c.revert(It.isAny())).callback(async()=>cmd4reverted=true);

        let cmd5 = Mock.ofType<IRevertableCommand>();
        cmd5.setup(c => c.execute(It.isAny())).callback(()=>pipelineCompleted = true);
        cmd5.setup(c => c.revert).returns(()=>(a?:any)=>a);
        cmd5.setup(c => c.revert(It.isAny())).callback(async()=>cmd5reverted=true);

        let cmd = new RevertableMacroCommand();
        cmd.add(cmd1.object);
        cmd.add(cmd2.object);
        cmd.add(cmd3.object);
        cmd.add(cmd4.object);
        cmd.add(cmd5.object);

        let error = undefined;

        try
        {
            await cmd.execute();
            assert.fail('This statement should not be executed.');
        }
        catch(e)
        {
            error = e;
        }

        assert.instanceOf(error, Error);
        assert.isTrue((<string>error.message).includes('Simulated Error'));
        assert.isFalse(pipelineCompleted);

        assert.isTrue(cmd1reverted);
        assert.isTrue(cmd3reverted);
        assert.isFalse(cmd4reverted);
        assert.isFalse(cmd5reverted);
        assert.isFalse(pipelineCompleted);
    });

    it('execute() - work with a promise', async () =>
    {
        let pipelineCompleted = false;
        let cmd1reverted = false;
        let cmd2reverted = false;
        let cmd3reverted = false;

        let cmd1 = Mock.ofType<IRevertableCommand>();
        cmd1.setup(c => c.execute(undefined)).returns(async ()=>true);
        cmd1.setup(c => c.revert).returns(()=>(a?:any)=>a);
        cmd1.setup(c => c.revert(It.isAny())).callback(async()=>cmd1reverted=true);

        let cmd2 = Mock.ofType<IRevertableCommand>();
        cmd2.setup(c => c.execute(true)).returns(()=>new Promise((resolve)=>setTimeout(() =>resolve(2), 10)));
        cmd2.setup(c => c.revert).returns(()=>(a?:any)=>a);
        cmd2.setup(c => c.revert(It.isAny())).callback(async()=>cmd2reverted=true);

        let cmd3 = Mock.ofType<IRevertableCommand>();
        cmd3.setup(c => c.execute(2)).callback(()=>pipelineCompleted = true).returns(async ()=>"done");
        cmd3.setup(c => c.revert).returns(()=>(a?:any)=>a);
        cmd3.setup(c => c.revert(It.isAny())).callback(async()=>cmd3reverted=true);

        let cmd = new RevertableMacroCommand();
        cmd.add(cmd1.object);
        cmd.add(cmd2.object);
        cmd.add(cmd3.object);

        let result = await cmd.execute();
        
        assert.equal(result, "done");

        assert.isFalse(cmd1reverted);
        assert.isFalse(cmd2reverted);
        assert.isFalse(cmd3reverted);
        assert.isTrue(pipelineCompleted);
    });
});