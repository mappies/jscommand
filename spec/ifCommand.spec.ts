import {assert as assert} from 'chai';
import {Mock, It} from "typemoq";
import { ICommand } from '../src/interfaces/iCommand';
import { Command } from '../src/entities/command';
import { IfCommand } from '../src/entities/ifCommand';
import { IRevertableCommand } from '../src/interfaces/iRevertableCommand';

class EchoCommand extends Command 
{
    constructor(private value:any)
    {
        super();
    }

    action(data?: any):any {
        return this.value;
    }
}


describe('IfCommand', () => 
{  
    it('execute() - true condition - a command', async () =>
    {   
        let called = false;

        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(new EchoCommand(true), mockedCommand.object).execute();
        
        assert.isTrue(called);
    });

    it('execute() - true condition - a boolean true', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(true, mockedCommand.object).execute();
        
        assert.isTrue(called);
    });

    it('execute() - true condition - an non-empty array', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(['false'], mockedCommand.object).execute();
        
        assert.isTrue(called);
    });

    it('execute() - true condition - a string', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand("x", mockedCommand.object).execute();
        
        assert.isTrue(called);
    });

    it('execute() - true condition - a positive number', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(1, mockedCommand.object).execute();
        
        assert.isTrue(called);
    });

    it('execute() - false condition - a command', async () =>
    {   
        let called = false;

        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(new EchoCommand(false), mockedCommand.object).execute();
        
        assert.isFalse(called);
    });

    it('execute() - false condition - a boolean false', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(false, mockedCommand.object).execute();
        
        assert.isFalse(called);
    });

    it('execute() - false condition - an empty array', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand([], mockedCommand.object).execute();
        
        assert.isFalse(called);
    });

    it('execute() - false condition - an empty string', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand("", mockedCommand.object).execute();
        
        assert.isFalse(called);
    });

    it('execute() - false condition - a zero', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(0, mockedCommand.object).execute();
        
        assert.isFalse(called);
    });

    it('execute() - false condition - a negative number', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(-1, mockedCommand.object).execute();
        
        assert.isFalse(called);
    });

    it('execute() - false condition - undefined', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(undefined, mockedCommand.object).execute();
        
        assert.isFalse(called);
    });

    it('execute() - false condition - null', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(null, mockedCommand.object).execute();
        
        assert.isFalse(called);
    });

    it('execute() - false condition - NaN', async () =>
    {   
        let called = false;
        let mockedCommand = Mock.ofType<ICommand>();
        mockedCommand.setup(c => c.execute(It.isAny())).callback(()=>called=true);
        
        await new IfCommand(NaN, mockedCommand.object).execute();
        
        assert.isFalse(called);
    });

    it('revert() - not revertable command', async () =>
    { 
        let mockedCommand = Mock.ofType<ICommand>();
        
        let command =  new IfCommand(true, mockedCommand.object);

        let error;

        try
        {
            await command.execute();
            await command.revert();
        }
        catch(e)
        {
            error = e;
        }

        assert.isUndefined(error);
    });

    it('revert() - condition true - reverted', async () =>
    {   
        let called = false;

        let mockedCommand = Mock.ofType<IRevertableCommand>();
        mockedCommand.setup(c => c.revert(It.isAny())).callback(()=>called=true);
        
        let command =  new IfCommand(true, mockedCommand.object);

        await command.execute();
        assert.isFalse(called);

        await command.revert();
        assert.isTrue(called);
    });

    it('revert() - condition false - not reverted', async () =>
    {   
        let called = false;

        let mockedCommand = Mock.ofType<IRevertableCommand>();
        mockedCommand.setup(c => c.revert(It.isAny())).callback(()=>called=true);
        
        let command =  new IfCommand(false, mockedCommand.object);

        await command.execute();
        assert.isFalse(called);

        await command.revert();
        assert.isFalse(called);
    });
});

