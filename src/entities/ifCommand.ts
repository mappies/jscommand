import { Command } from "./command";
import { ICommand } from "../interfaces/iCommand";
import { IRevertableCommand } from '../interfaces/iRevertableCommand';
import { RevertableCommand } from "./revertableCommand";

export class IfCommand extends RevertableCommand 
{
    private executed: boolean;

    constructor(public condition:any, public command:ICommand)
    {
        super();

        this.executed = false;
    }

    async action(data?: any): Promise<any>
    {
        let condition = this.condition;
        
        if(condition instanceof Command)
        {
            condition = await condition.execute(data);
        }

        if((condition !== null && typeof condition === 'object' && condition.length > 0) || (condition && !Array.isArray(condition) && !(typeof condition === 'number' && condition <= 0)))
        {
            this.executed = true;
            return this.command.execute(data);
        }
    }

    async revert(data?: any): Promise<any>
    {
        if(this.executed && (<IRevertableCommand> this.command).revert)
        {
            return await (<IRevertableCommand> this.command).revert(data);
        }
    }
};