import { Command } from './command';
import { ICommand } from '../interfaces/iCommand';
import * as AggregateError from 'aggregate-error';

export class OneOfCommand extends Command 
{
    constructor(public commands:ICommand[] = [])
    {
        super();
    }

    add(command:ICommand): OneOfCommand
    {
        this.commands.push(command);

        return this;
    }

    async action(data?: any): Promise<any>
    {
        let errors = [];

        for(let command of this.commands)
        {
            try
            {
                return await command.execute(data);
            }
            catch(e)
            {
                errors.push(e);
            }
        }

        if(errors.length > 0)
        {
            throw new AggregateError(errors);
        }
    }
};