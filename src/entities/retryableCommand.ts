import { ICommand } from "../interfaces/iCommand";
import * as AggregateError from 'aggregate-error';
import { RevertableCommand } from "./revertableCommand";
import { IRevertableCommand } from "../interfaces/iRevertableCommand";

export class RetryableCommand extends RevertableCommand
{
    constructor(public command:ICommand, private ifErrorIs:any, private retryHowManyTimes:number = 1)
    {
        super();
    }
    
    async action(data?: any): Promise<any>
    {
        let count = this.retryHowManyTimes;

        let errors = [];

        do
        {
            try
            {
                return await this.command.action(data);
            }
            catch(e)
            {
                if( !(e === this.ifErrorIs 
                      || (typeof this.ifErrorIs === 'object' && e instanceof this.ifErrorIs)
                      || (e.constructor 
                          && this.ifErrorIs.constructor 
                          && (e.constructor.name === this.ifErrorIs.constructor.name 
                              || e.constructor.name === this.ifErrorIs.name))))
                {
                    throw e;
                }

                errors.push(e);
            }
        }
        while(--count >= 0);

        throw new AggregateError(errors);
    }

    async revert(data?: any): Promise<any>
    {
        if((<IRevertableCommand>this.command).revert)
        {
            return await (<IRevertableCommand>this.command).revert(data);
        }
    }
};