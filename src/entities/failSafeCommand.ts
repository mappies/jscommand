import { ICommand } from "../interfaces/iCommand";
import { RevertableCommand } from "./revertableCommand";
import { IRevertableCommand } from "../interfaces/iRevertableCommand";

export class FailSafeCommand extends RevertableCommand 
{
    constructor(public command:ICommand)
    {
        super();
    }

    async action(data?: any): Promise<any>
    {
        try
        {
            return await this.command.execute(data);
        }
        catch{}
    }

    async revert(data?: any): Promise<any>
    {
        if((<IRevertableCommand> this.command).revert)
        {
            return await (<IRevertableCommand> this.command).revert(data);
        }
    }
};