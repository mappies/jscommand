import { IRevertableCommand } from '../interfaces/iRevertableCommand';
import { ICommand } from '../interfaces/iCommand';
import * as AggregateError from 'aggregate-error';
import { MacroCommand } from './macroCommand';
import { Command } from './command';

export class RevertableMacroCommand extends Command implements IRevertableCommand
{
    executedIndex:number;

    constructor(public commands:ICommand[] = [])
    {
        super();
    }

    add(command:ICommand): MacroCommand
    {
        this.commands.push(command);

        return this;
    }

    async action(data?: any): Promise<any>
    {
        let result = data;
        try
        {
            this.executedIndex = 0;

            for(let command of this.commands)
            {
                result = await command.execute(result);

                this.executedIndex++;
            }

            return result;
        }
        catch(e1)
        {
            let errors = [e1];

            try
            {
                await this.revert(result);
            }
            catch(e2)
            {
                errors.push(e1);
            }

            throw new AggregateError(errors);
        }
    }

    async revert(data?: any): Promise<any>
    {
        let errors = [];
        
        for(let i = this.executedIndex - 1; i >= 0; i--)
        {
            let command = this.commands[i];
            
            if((<IRevertableCommand> command).revert)
            {
                try
                {
                    data = await (<IRevertableCommand> command).revert(data);
                }
                catch(e)
                {
                    errors.push(e);
                }
            }
        }

        if(errors.length > 0)
        {
            throw new AggregateError(errors);
        }
    }
};