import { Command } from './command';
import { IRevertableCommand } from '../interfaces/iRevertableCommand';
import * as AggregateError from 'aggregate-error';

export abstract class RevertableCommand extends Command implements IRevertableCommand
{
    abstract revert(data?: any): any;

    execute(data?: any): any
    {
        try
        {
            return this.action(data);
        }
        catch(e1)
        {
            let errors = [e1];
            
            try
            {
                this.revert(data);
            }
            catch(e2)
            {
                errors.push(e2);
            }
            
            throw errors.length === 1 ? errors[0] : new AggregateError(errors);
        }
    }
};