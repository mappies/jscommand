import { RevertableCommand } from "./revertableCommand";

export class AnonymousCommand extends RevertableCommand
{
    constructor(private actionFunction:Function, private revertFunction?:Function)
    {
        super();
    }

    action(data?: any)
    {
        return this.actionFunction(data);
    }

    revert(data?: any)
    {
        if(this.revertFunction) return this.revertFunction(data);
    }    
    
}