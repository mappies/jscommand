import { Command } from './command';
import { ICommand } from '../interfaces/iCommand';

export class MacroCommand extends Command 
{
    constructor(public commands:ICommand[] = [])
    {
        super();
    }

    add(command:ICommand): MacroCommand
    {
        this.commands.push(command);

        return this;
    }

    async action(data?: any): Promise<any>
    {
        let result = data;

        for(let command of this.commands)
        {
            result = await command.execute(result);
        }

        return result;
    }
};