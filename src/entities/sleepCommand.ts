import { Command } from "./command";

export class SleepCommand extends Command 
{
    constructor(private fromMs:number, private toMs?:number)
    {
        super();
    }

    async action(data?: any): Promise<any>
    {
        if(this.fromMs && this.toMs)
        {
            let ms = Math.floor(Math.random()*(this.toMs-this.fromMs+1)+this.fromMs);

            return new Promise((resolve, reject) => setTimeout(resolve, ms));
        }
        else
        {
            return new Promise((resolve, reject) => setTimeout(resolve, this.fromMs));
        }
    }
};