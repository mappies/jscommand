import { ICommand } from '../interfaces/iCommand';

export abstract class Command implements ICommand 
{
    abstract action(data?: any);

    execute(data?:any): any
    {
        return this.action(data);
    }
};