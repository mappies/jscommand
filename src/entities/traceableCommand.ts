import { RevertableCommand } from "./revertableCommand";
let StopWatch = require('agstopwatch');
import { ICommand } from "../interfaces/iCommand";
import { IRevertableCommand } from '../interfaces/iRevertableCommand';
import { MacroCommand } from './macroCommand';
import { RevertableMacroCommand } from './revertableMacroCommand';
import { RetryableCommand } from "./retryableCommand";
import { IfCommand } from "./ifCommand";
import { Command } from './command';
import { FailSafeCommand } from './failSafeCommand';
import { OneOfCommand } from './oneOfCommand';

export class TraceableCommand extends RevertableCommand
{
    constructor(private command:ICommand, private indentation:number = 0)
    {
        super();

        if(command instanceof MacroCommand || command instanceof RevertableMacroCommand || command instanceof OneOfCommand || Array.isArray(command['commands']))
        {
            for(let i=0; i<command['commands'].length; i++)
            {
                command['commands'][i] = new TraceableCommand(command['commands'][i], indentation + 1);
            }
        }
        else if(command instanceof RetryableCommand)
        {
            (<RetryableCommand>this.command).command = new TraceableCommand((<RetryableCommand>this.command).command, indentation + 1);
        }
        else if(command instanceof IfCommand)
        {
            command.condition = command.condition instanceof Command ? new TraceableCommand(command.condition, indentation + 1) : command.condition;
            command.command = new TraceableCommand(command.command, indentation + 1);
        }
        else if(command['command'])
        {
            command['command'] = new TraceableCommand(command['command'], indentation + 1);
        }
    }
    
    async action(data?: any): Promise<any>
    {
        let watch = new StopWatch();
        watch.start();

        try
        {
            let result = await this.command.action(data);

            watch.stop();

            console.log(`${' '.repeat(this.indentation * 2)} + ${this.command.constructor.name} [${watch.elapsed}ms]`);

            return result
        }
        catch(e)
        {
            watch.stop();

            console.log(`${' '.repeat(this.indentation * 2)} ! ${this.command.constructor.name} [${watch.elapsed}ms]`)

            throw e;
        }
    }

    async revert(data?: any): Promise<any>
    {
        let watch = new StopWatch();
        watch.start();

        try
        {
            let result = await (<IRevertableCommand>this.command).revert(data);

            watch.stop();

            console.log(`${' '.repeat(this.indentation * 2)} - ${this.command.constructor.name} [${watch.elapsed}ms]`)

            return result;
        }
        catch(e)
        {
            watch.stop();

            console.log(`${' '.repeat(this.indentation * 2)} x ${this.command.constructor.name} [${watch.elapsed}ms]`)
        }
    }  
};