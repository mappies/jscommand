import { ICommand } from './iCommand';

export interface IRevertableCommand extends ICommand 
{
    revert(data?:any): Promise<any>;
};