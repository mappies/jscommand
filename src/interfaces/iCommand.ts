export interface ICommand
{
    action(data?:any): Promise<any>;
    execute(data?:any): Promise<any>;
};