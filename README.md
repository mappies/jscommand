# jscommand

Command pattern for JavaScript

### Create a command

To create a command, create a class that inherits from *Command*.
```cs
class Add extends Command
{
    constructor(private a:number)
    {
        super();
    }

    action(data:any): any
    {
        return data + this.a;
    }
}

let command = new Add(3);
await command.execute(2); //Return 5
```

### Conditional command

A command can be executed conditionally by using *IfCommand*.

```cs
await new IfCommand(false, new Add(2)).execute(1); // Returns undefined
await new IfCommand(true, new Add(2)).execute(1); // Returns 3
```

### Chain commands

Commands can be chained and executed in as a single unit by using *MacroCommand*.

```cs
let commands = new MacroCommand();
commands.add(new Add(3));
commands.add(new Add(4));
commands.add(new Add(5));

await commands.execute(0); //Returns 12
```

### Rollback commands

A command can be rolled back if it inherits from *RevertableCommand*.

```cs
class Add extends RevertableCommand
{
    constructor(private a:number)
    {
        super();
    }

    action(data:any): any
    {
        if(data > 2) throw new Error('Overflow!')
        return data + this.a;
    }

    revert(data:any): any
    {
        console.log('Rollback')
    }
}

let commands = new RevertableMacroCommand();
commands.add(new Add(1));
commands.add(new Add(1));
commands.add(new Add(1));
commands.add(new Add(1));

await commands.execute(0); 

```

### Logging command

A command execution can be logged by wrapping the command with *TraceableCommand*

```cs

new TraceableCommand(new Add(3)).execute(1); //Returns 4 and execution information is logged to console.

```

### Fail-safe command

To supporess an error from a command, wrap the command with *FailSafeCommand*

```cs
new FailSafeCommmand(new CouldThrowAnErrorCommand()).execute();
```


### Retryable command

A command can be retried if an expected error occurs if the command is wrapped with *RetryableCommand*.

```cs
new RetryableCommand(new Add(3), Error, 3).execute(0); //The command Add() will be executed. If an error of type `Error` occurs, Add() will be re-executed 3 more times.
```


### Sleep command

A sleep command sets a ranom timeout between to millisecond valuesCommands can be chained to perform complex operations.

```cs
let minMsWait = 50; // 50 milliseconds
let maxMsWait = 1500; // 1500 milliseconds
new SleepCommand(minMsWait, maxMsWait).execute(0); 
```


### Function-based command

For simplicity, *AnonymousCommand* can be used to wrap a function so that a function can act as a command.

```cs
let command = new AnonymousCommand((a)=>a+1);
await command.execute(2); //Returns 3
```

### Nested commands

Commands can be chained to perform complex operations.

```cs
let commands = new MacroCommand();
commands.add(new IfCommand(new AnonymousCommand((a)=>a+1 > 2), new Add(1));
commands.add(new RetryableCommand(new Add(2)));

new TraceableCommand(commands).execute(0); 
```

### One-of commands

A OneOf command executes the given commands in order and will return once 
a command executed successfully.  The remaining commands will not be executed.

```cs
let commands = new OneOfCommand();
commands.add(new DoSomeThingCommand());
commands.add(new TryThisIfTheFirstCommandFailed()));
commands.add(new TryThisIfTheSecondCommandFailed()));

new TraceableCommand(commands).execute(); 
```